(ns queues.core-test
  (:require 
            [clojure.data.json :as djson]
            [queues.core  :as core]
            [midje.sweet :refer :all]
            ))

(def filename "resources/input.1.json")
(def input_file (djson/read-str (slurp filename) :key-fn keyword ))
(def jobs (remove nil? (map :new_job input_file)))
(def jobs_request (remove nil? (map :job_request input_file)))
(def agents (remove nil? (map :new_agent input_file)))
(def jobs_agents (core/get-jobs-agents jobs agents))

(fact "test core/header-a"
    (core/header-a "test") => {:new_agent "test"}
)

(fact "test core/header-j"
    (core/header-j "test") => {:new_job "test"}
)

(fact "test core/header-jr"
    (core/header-jr "test") => {:job_request "test"}
)

(fact "test core/urgent-jobs"
    (core/urgent-jobs jobs) => (list 
                                  (hash-map :id "c0033410-981c-428a-954a-35dec05ef1d2" 
                                  :type "bills-questions" :urgent true))
)

(fact "test core/non-urgent-jobs"
    (core/non-urgent-jobs jobs) => (list 
                                  (hash-map :id "f26e890b-df8e-422e-a39c-7762aa0bac36"
                                  :type "rewards-question" :urgent false)
                                  (hash-map :id "690de6bc-163c-4345-bf6f-25dd0c58e864"
                                   :type "bills-questions" :urgent false)
                                  )
)

(fact "test core/get-a-ps-elements"
    (core/get-a-ps-elements (first jobs_agents)) => (list "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88")
)

(fact "test core/get-a-ps-list"
    (count (core/get-a-ps-list jobs_agents)) => 3
)

(fact "test core/union-itens"
     (core/union-itens jobs_agents 0) => (into #{} ["8ab86c18-3fae-4804-bfd9-c3d6e8f66260" 
                                           "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"]) 
)

(fact "test core/get-diff-ag"
     (core/get-diff-ag jobs_agents) => (into #{} ["8ab86c18-3fae-4804-bfd9-c3d6e8f66260" 
                                           "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"]) 
)

