(ns queues.core
    (:require [clojure.data.json :as djson])
    (:require [clojure.java.io :as io])
    (:require [clojure.set :refer [union]])
    (:require [clojure.data :as dt])
)

(defn header-a [x] 
  "puts a tag :new_agent in the job(parameter)"
  (zipmap [:new_agent] [x]) )

(defn header-j [x] 
  "puts a tag :new_job in the job(parameter)"
  (zipmap [:new_job] [x]) )

(defn header-jr [x] 
  "puts a tag :job_requestent in the job(parameter)"
  (zipmap [:job_request] [x]) )

(defn urgent-jobs [job-list]
 "get list of urgent jobs"
  (filter (comp true? :urgent) job-list) )

(defn non-urgent-jobs [job-list]
 "get list of non urgent jobs"
  (filter (comp false? :urgent) job-list) )

(defn get-job [j_list]
 "get next job of job's list "
 (if (>  (count (urgent-jobs j_list )) 0 )
     (first (urgent-jobs j_list))
     (first j_list)))

(defn get-a-ps-elements [x]
    "this function gets the list of job_agent's primary skills"
    (if (> (get x :n_ag_p) 1)
      (get x :ag_ps)
      (list (get x :ag_ps)  )
    ))

(defn get-a-ps-list [lista]
  "this function gets the list of primary skills from a complete jobs_agents list"
  (map (fn [x] (get-a-ps-elements x)) lista )
)

(defn union-itens [lista  i]
  "funcao que faz a uniao do item i da lista com o item i+1 "
  (clojure.set/union (set (nth (get-a-ps-list  lista ) i )) (set(nth (get-a-ps-list  lista ) (inc i) )))
) 

(defn get-diff-ag [ja_list]
    "this function returns list of different elements"
    (loop [i 0 jal ja_list al [] ]
       (when (< i (- (count jal) 1))
          (if (= (inc i)  (- (count jal) 1))  
            (clojure.set/union (union-itens jal i) al)
            (recur 
              (inc i) 
              jal 
              (clojure.set/union (union-itens jal i) al))))))

(defn get-agent [al_js jr_list] 
  (loop [i 0 ] 
    (when (< i (count al_js)) 
      (if (.contains (map (fn [x] (get x :agent_id) ) jr_list) (nth (into []  al_js) i))
          (nth (into []  al_js) i)
        (recur (inc i) )
      ))))

(defn ordernar [jobs_assigned] 
  (map  (fn [x] (zipmap [:job_assigned] [x] )) (sort-by :priority jobs_assigned))
)
(defn get-ag-jr [a_list nr_ag jr_list]
"this function checks to see if there is a job-request for the list of agents passed as a parameter. returns only agents that have job_request"
     (if (empty? a_list)
      []
      (clojure.set/intersection  
          (set (map (fn[x] (get x :agent_id)) jr_list)) 
          (set (if (> nr_ag 1)
              a_list 
              (if (= java.lang.String (type a_list) )
                (list a_list)   
                a_list
              )))))) 

(defn result [msg o_list]
  "write result in files and print message"
  (do
    ;(spit "resources/remaining.json" (djson/write-str (list  (map fn1 list1) (map fn2 list2))))
    (spit "resources/output.json" (djson/write-str (ordernar o_list)))

    (println "Generated files:resources/output.json" )
    (println msg)))

(defn attach-jobs-agent [agents j ja] 
 "Auxiliary function for the get-jobs-agents function which assembles the jobs_agents's list which means the list of jobs associated with the agents which are able to perform each job" 
 (do
      (let [jobs-agents ja]
        (if
          (empty?  agents)
            (do
              jobs-agents)
            (do
              (let [a (first agents)] 
                (if (.contains  (get a :primary_skillset)  (get j :type)) ; se contem no primario adiciona a lista de job
                    (if (contains? j :ag_ps)
                        (if (not (.contains (get j :ag_ps) (get a :id) ))     
                            (do
                              (get j :ag_ps)
                              (recur 
                                (disj (set agents) a) 
                                (assoc  j :ag_ps (list (get a :id) (get j :ag_ps)) :n_ag_p (count (list (get a :id) (get j :ag_ps))))
                                (assoc  j :ag_ps (list (get a :id) (get j :ag_ps)) :n_ag_p (count (list (get a :id) (get j :ag_ps)))) )))
                        (do
                          (recur 
                            (disj (set agents) a) 
                            (assoc  j :ag_ps  (get a :id) :n_ag_p 1) 
                            (assoc  j :ag_ps  (get a :id) :n_ag_p 1))))
                    (do  ; se  nao contem no primario 
                      (if (.contains  (get a :secondary_skillset)  (get j :type)) ; verifica se contem no secundario 
                        (if (contains? j :ag_ss)
                          (if (not (.contains (get j :ag_ss) (get a :id) ))     
                            (do
                              (get j :ag_ss)
                              (recur 
                                (disj (set agents) a) 
                                (assoc  j :ag_ss  (list (get a :id) (get j :ag_ss)) :n_ag_s (count (list (get a :id) (get j :ag_ss)))  )
                                (assoc  j :ag_ss  (list (get a :id) (get j :ag_ss)) :n_ag_s (count (list (get a :id) (get j :ag_ss))) ))))
                            (do
                              (recur 
                                (disj (set agents) a) 
                                (assoc  j :ag_ss  (get a :id) :n_ag_s 1) 
                                (assoc  j :ag_ss  (get a :id) :n_ag_s 1 ))))
                        (recur ; se nao contem nem no primario e no secundario 
                            (disj (set agents) a) 
                            j 
                            j  ))))))))))

(defn get-jobs-agents [jobs agents] 
  "Associate job with all agent is allowed to perform this job."
    (for [j jobs] 
        (attach-jobs-agent agents j nil)))

(defn selecting-jobs [ja_list jr_qtd jobs_selected p jr_list]
  "function that mounts the list of jobs selected for job_request"
    (if (< (count jobs_selected) jr_qtd)
      (let [job (get-job ja_list) ]
          (let [a_jr (get-ag-jr (get job :ag_ps) (get job :n_ag_p) jr_list)] ; intercessao da lista de agentes com a lista de job_request
              (if (> (count a_jr)  0) ; verifica se a lista de agentes do job está na lista de job_request
                (if (= nil jobs_selected) ; se for o primeiro nao precisa contar para ver se count de :ag_ps  é > ou = ao  numero de jobs 
                  (recur
                        (remove (fn [x] (= job x)) ja_list)
                        jr_qtd 
                        (list (assoc job :selected true :priority p :ag_ps a_jr))
                        (inc p)
                        jr_list
                        )
                   ; se nao for o primeiro, ou seja a partir do segundo item, avaliar se count de :ag_ps  é > ou = ao numero de jobs  
                  (if (>= (compare  (count (get-diff-ag (conj jobs_selected job ))) (count  (conj jobs_selected job))) 0)  
                    (recur
                      (remove (fn [x] (= job x)) ja_list)
                      jr_qtd 
                      (conj jobs_selected (assoc job :selected true :priority p :ag_ps a_jr) )
                      (inc p)
                      jr_list
                      )))
                ;se  a qtd de agentes distintos for menor que a quantidade de jobs escolher outro job 
                ;se nao tiver nenhuma seleção possivel, procurar na lista de skills secundários
            )))
      (conj jobs_selected ja_list)))
      
(defn execute-jobs-requests [js_list jr_list qtd out_list]
    "function responsible for assigning job to agent"
    (do 
      (if (> (count jr_list) 0 ) 
          (if (> (count js_list) 0 )
            (let [job (first js_list) ] ; inicia pelo job que contem menos agentes capazes de realiza-lo
              (let [al_js (get-ag-jr  (get job :ag_ps) (get job :n_ag_p) jr_list)] ; al_js - agent list job select
                (let [ag (get-agent al_js jr_list)]
                  (do
                    (execute-jobs-requests
                        (remove (fn [x] (= (get job :id) (get x :id) ))  js_list) 
                        (remove (fn [x] (= (get x :agent_id) ag)) jr_list)
                        qtd
                        (if (nil? out_list) 
                               (zipmap [:job_id  :agent_id :priority] [(get job :id)  ag (get job :priority) ])
                               (list out_list  (zipmap [:job_id  :agent_id :priority] [(get job :id) ag (get job :priority)]) )))
            ))))
            (result "Error: Missing jobs for job_request !" out_list  ))
          (do
            (if (= (count out_list) qtd )
              (result "Success!" out_list  )
              (result "Error: Missing jobs for job_request !" out_list  )
            )))))

(defn dequeue[filename]
  "initial function"
  (do
   ; (def filename "resources/input.1.json")
    (def input_file (djson/read-str (slurp filename) :key-fn keyword ))
    (def jobs (remove nil? (map :new_job input_file)))
    (def jobs_request (remove nil? (map :job_request input_file)))
    (def agents (remove nil? (map :new_agent input_file)))
    (def jobs_agents (get-jobs-agents jobs agents))
    (def jobs_selected (selecting-jobs jobs_agents (count jobs_request) nil 1 jobs_request) )
    
    ;(println jobs_selected)
    (def jobs_by_qtd_ag (filter (comp true? :selected) (sort-by :n_ag_p jobs_selected)))
    
    (execute-jobs-requests jobs_by_qtd_ag jobs_request (count jobs_request) nil)
    
    "end"
  )
)