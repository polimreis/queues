# queues

## About

A *job* is any task that needs to get done. It has a unique id, a type - which denotes the category of that job -, and
an urgency (boolean) flag - indicating whether that job has a high priority.

An *agent* is someone that performs a job. They also have a unique id and two disjoint skill sets: primary and
secondary. Skill sets are a simple list of job types that an agent is allowed to perform.

The dequeue function that abides to these rules:

1.1 You can assume the list of jobs passed in is ordered by the time the they have entered the system.

1.2 Jobs that arrived first should be assigned first, unless it has a "urgent" flag, in which case it has a higher
  priority.

1.3 A job cannot be assigned to more than one agent at a time.

1.4 An agent is not handed a job whose type is not among its skill sets.

1.5 An agent only receives a job whose type is contained among its secondary skill set if no job from its primary skill set is available. This rule takes precedence over the "urgent" rule.



### TO-DO

- Complement function in line 167, to find in secondary skills and reselect jobs, when number of distinct agents in selected jobs is smaller than number of selected jobs.

- Format the output file tab

- Create more test cases

## Usage

I have created 4 examples of input :

2 files will succeed:

* resources/input.1.json  *(initial sample)*
* resources/input.2.json  *(another correct sample)* 

2 files will present error

* resources/input.3.json *(missing agents)*
* resources/input.4.json *(missing jobs)*

``` (dequeue "resources/input.1.json")
```

The program will generate one file:

* resources/output.json   *(jobs alocated to agents)*


